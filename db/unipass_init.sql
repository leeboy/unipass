DROP TABLE IF EXISTS `site_code`;

CREATE TABLE `site_code` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) NOT NULL,
  `site_domain` varchar(50) NOT NULL,
  `site_salt` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
